namespace FilmBuffs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveReferenceMovieToPeople : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Movies", "PeopleID", "dbo.People");
            DropIndex("dbo.Movies", new[] { "PeopleID" });
            AddColumn("dbo.Movies", "Director", c => c.String());
            DropColumn("dbo.Movies", "PeopleID");
            DropTable("dbo.People");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.People",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Year = c.Int(nullable: false),
                        Bio = c.String(),
                        Role = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Movies", "PeopleID", c => c.Int(nullable: false));
            DropColumn("dbo.Movies", "Director");
            CreateIndex("dbo.Movies", "PeopleID");
            AddForeignKey("dbo.Movies", "PeopleID", "dbo.People", "Id", cascadeDelete: true);
        }
    }
}
