﻿
using FilmBuffs.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmBuffs.Areas.Manage.Models
{
    public class GenreModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public GenreModel() { }

        public GenreModel(Genre entity)
        {
            Id = entity.Id;
            Name = entity.Name;
        }
        public bool IsValid()
        {
            if (Id == 0)
            {
                return false;
            }
            if (string.IsNullOrWhiteSpace(Name))
            {
                return false;
            }

            return true;
        }

        public Genre ToEntity()
        {
            return new Genre
            {
                Id = Id,
                Name = Name
            };
        }
    }
}