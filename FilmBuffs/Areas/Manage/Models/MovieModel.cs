﻿using FilmBuffs.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FilmBuffs.Areas.Manage.Models
{
    public class MovieModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Year { get; set; }
        public string Director { get; set; }

        [AllowHtml]
        public string Plot { get; set; }
        public int GenreID { get; set; }

        public string GenreName { get; set; }
        public MovieModel()
        {

        }

        public MovieModel(Movie entity)
        {
            Id = entity.Id;
            Title = entity.Title;
            Year = entity.Year;
            Director = entity.Director;
            Plot = entity.Plot;
            if (entity.Genres != null)
            {
                GenreID = entity.GenreID;
                GenreName = entity.Genres.Name;
            }
        }

        public bool IsValid()
        {
            if (string.IsNullOrWhiteSpace(Title))
            {
                return false;
            }

            if (Year < 1800)
            {
                return false;
            }

            if (GenreID == 0)
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(Director))
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(Plot))
            {
                return false;
            }

            return true;
        }

        public Movie ToEntity()
        {
            return new Movie
            {
                Id = Id,
                Title = Title,
                Year = Year,
                Director = Director,
                Plot = Plot,
                GenreID = GenreID
        };
        }

    }
}