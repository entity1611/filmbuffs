﻿using FilmBuffs.Areas.Manage.Models;
using FilmBuffs.Database;
using FilmBuffs.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FilmBuffs.Areas.Manage.Controllers
{
    public class ManageMovieController : BaseController
    {
        // GET: Manage/Movie
        public ActionResult Index(int? pageIndex, string orderBy, string keyword)
        {
            RequireAdminUser();
            if (pageIndex ==null || pageIndex < 1)
            {
                pageIndex = 1;
            }
            
            var model = new PagingModel<MovieModel>
            {              
                CurrentPage = pageIndex.Value,
                PageSize = 10,
                OrderBy = orderBy == null? string.Empty : orderBy.ToLower(),
                Keyword = keyword == null ? string.Empty : keyword.ToLower(),
                Records = new List<MovieModel>()

            };

            var dbContext = new FbDatabase();
            var dbMovies = dbContext.Movies.ToList();
            var moviesFromDatabase = dbContext.Movies.AsEnumerable();
            
            //search
            if(!string.IsNullOrEmpty(model.Keyword))
            {
                model.Keyword = model.Keyword.Trim().ToLower();
                moviesFromDatabase = moviesFromDatabase.Where(e => e.Title.ToLower().Contains(model.Keyword) ||
                                 e.Id.ToString() == model.Keyword || e.Year.ToString() == model.Keyword).ToList();
            }
            

            //ordering
            switch (model.OrderBy)
            {
                case "title":
                    moviesFromDatabase = moviesFromDatabase.OrderBy(e => e.Title).ToList();
                    break;
                case "year":
                    moviesFromDatabase = moviesFromDatabase.OrderByDescending(e => e.Year).ToList();
                    break; 
                default:
                    moviesFromDatabase = moviesFromDatabase.OrderByDescending(e => e.Id).ToList();
                    break;
            }


            model.TotalRecord = moviesFromDatabase.Count();
            var dbMovieList = moviesFromDatabase.Skip((model.CurrentPage - 1) * model.PageSize).Take(model.PageSize).ToList();
            model.Records = dbMovieList.Select(e => new MovieModel(e)).ToList();
            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            RequireAdminUser();
            var context = new FbDatabase();
            ViewBag.Genres = context.Genres.ToList().Select(e => new GenreModel(e));
            return View("Create");
        }

        [HttpPost]
        public ActionResult Create(MovieModel movie)
        {
            RequireAdminUser();
            var context = new FbDatabase();
            ViewBag.Genres = context.Genres.ToList().Select(e => new GenreModel(e));
            if (ModelState.IsValid)
            {
                if (movie.IsValid())
                {
                    var dbContext = new FbDatabase();
                    dbContext.Movies.Add(movie.ToEntity());
                    dbContext.SaveChanges();

                    return RedirectToAction("Index");
                }
                else
                {
                    //errorMessage
                    ViewBag.ErrorMessage = "Invalid data. Please try again";
                }
            }
            else
            {
                //errorMessage
                ViewBag.ErrorMessage = "An error occured. Please try again.";
            }
            return View("Create");
        }

        [HttpGet]
        public ActionResult Update(int id)
        {
            RequireAdminUser();
            if (id > 0)
            {
                var dbContext = new FbDatabase();
                var entity = dbContext.Movies.FirstOrDefault(e => e.Id == id);
                if (entity != null)
                {
                    var model = new MovieModel(entity);

                    ViewBag.Genres = dbContext.Genres.ToList().Select(e => new GenreModel(e));

                    return View(model);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return View("Update");
        }

        [HttpPost]
        public ActionResult Update(MovieModel movie)
        {
            RequireAdminUser();
            if (ModelState.IsValid)
            {
                if (movie.IsValid())
                {
                    var dbContext = new FbDatabase();
                    var entity = dbContext.Movies.FirstOrDefault(e => e.Id == movie.Id);
                    if (entity != null)
                    {
                        entity.Title = movie.Title;
                        entity.Year = movie.Year;
                        entity.Director = movie.Director;
                        entity.Plot = movie.Plot;
                        entity.GenreID= movie.GenreID;

                        dbContext.SaveChanges();
                    }


                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.ErrorMessage = "Invalid data, please try again";
                }
            }
            else
            {
                ViewBag.ErrorMessage = "An error occured, try again later";
            }
            return View("Update");
        }

        [HttpPost]
        public ActionResult Delete(int id, string redirect)
        {
            RequireAdminUser();
            if (id > 0)
            {
                var dbContext = new FbDatabase();
                var entity = dbContext.Movies.FirstOrDefault(e => e.Id == id);
                if (entity != null)
                {
                    dbContext.Movies.Remove(entity);
                    dbContext.SaveChanges();
                }
            }
            return Redirect(redirect);
        }
    }

}