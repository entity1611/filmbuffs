﻿using FilmBuffs.Areas.Manage.Models;
using FilmBuffs.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FilmBuffs.Areas.Manage.Controllers
{
    public class ManageGenreController : BaseController
    {
        // GET: Manage/ManageGenre
        public ActionResult Index(int? pageIndex, string orderBy, string keyword)
        {
            RequireAdminUser();
            if (pageIndex == null || pageIndex < 1)
            {
                pageIndex = 1;
            }

            var model = new PagingModel<GenreModel>
            {
                CurrentPage = pageIndex.Value,
                PageSize = 10,
                OrderBy = orderBy == null ? string.Empty : orderBy.ToLower(),
                Keyword = keyword,
                Records = new List<GenreModel>()
            };

            var dbContext = new FbDatabase();
            var genreFromDatabase = dbContext.Genres.AsQueryable();

            //search by keyword
            if (!string.IsNullOrWhiteSpace(model.Keyword))
            {
                model.Keyword = model.Keyword.Trim().ToLower();

                genreFromDatabase = genreFromDatabase
                    .Where(e => e.Name.ToLower().Contains(model.Keyword) || e.Id.ToString() == model.Keyword);
            }

            model.TotalRecord = genreFromDatabase.Count();

            //order by
            switch (model.OrderBy)
            {
                case "name":
                    genreFromDatabase = genreFromDatabase.OrderByDescending(e => e.Name);
                    break;
                default:
                    genreFromDatabase = genreFromDatabase.OrderBy(e => e.Id);
                    break;
            }

            model.Records = genreFromDatabase
                .Skip((model.CurrentPage - 1) * model.PageSize)
                .Take(model.PageSize).ToList().Select(e => new GenreModel(e)).ToList();

            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            RequireAdminUser();
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns>JSON { Status: true/false, Message: "Error message here", Data: null }</returns>
        [HttpPost]
        public ActionResult Create(GenreModel model)
        {
            if (ModelState.IsValid)
            {
                //validate
                if (model.IsValid())
                {
                    var dbContext = new FbDatabase();

                    if (IsExist(dbContext, model.Name))
                    {
                        ViewBag.ErrorMessage = "The Genre have already existed.";
                    }

                    var newEntity = model.ToEntity();
                    dbContext.Genres.Add(newEntity);
                    dbContext.SaveChanges();

                    return RedirectToAction("Index");

                }
                else
                {
                    ViewBag.ErrorMessage = "Invalid data, please try again";
                }
            }
            else
            {
                ViewBag.ErrorMessage = "An error occured. Please try again";
            }
            return View();
        }

        [HttpGet]
        public ActionResult Update(int id)
        {
            RequireAdminUser();
            if (id > 0)
            {
                var dbContext = new FbDatabase();
                var entity = dbContext.Genres.FirstOrDefault(e => e.Id == id);
                if (entity != null)
                {
                    var model = new GenreModel(entity);

                    return View(model);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public ActionResult Update(GenreModel model)
        {
            RequireAdminUser();
            if (ModelState.IsValid)
            {
                //validate
                if (model.IsValid())
                {
                    var dbContext = new FbDatabase();
                    var entity = dbContext.Genres.FirstOrDefault(e => e.Id == model.Id);
                    if (entity != null)
                    {
                        if (model.Name.ToLower() != entity.Name.ToLower() && IsExist(dbContext, model.Name))
                        {
                            ViewBag.ErrorMessage = $"{model.Name} was exist, please pick another name";
                            return View(model);
                        }

                        entity.Name = model.Name;
                        dbContext.SaveChanges();
                    }

                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.ErrorMessage = "Invalid data, please try again";
                }
            }
            else
            {
                //show a error message
                ViewBag.ErrorMessage = "Unexpected error occurred, please try again later";
            }
            return View();
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            RequireAdminUser();
            if (id > 0)
            {
                var dbContext = new FbDatabase();
                var entity = dbContext.Genres.FirstOrDefault(e => e.Id == id);
                if (entity != null)
                {
                    dbContext.Genres.Remove(entity);
                    dbContext.SaveChanges();
                }
            }
            return Redirect("/");
        }

        bool IsExist(FbDatabase dbContext, string name)
        {
            return dbContext.Genres.Count(e => e.Name.ToLower() == name.ToLower()) != 0;
        }
    }
}