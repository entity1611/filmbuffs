﻿using FilmBuffs.Areas.Manage.Models;
using FilmBuffs.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FilmBuffs.Areas.Manage.Controllers
{
    public class AccountController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel input)
        {
            if (ModelState.IsValid)
            {
                //POST
                if (input.Username.ToLower() == "admin" && input.Password == "12345")
                {
                    //SESSION
                    SiteHelper.CurrentAdminUser = input;
                    return RedirectToAction("Index", "Manage");
                }
                else
                {
                    input.ErrorMessage = "Invalid username or password";
                    return View(input);
                }
            }

            //GET
            return View();
        }

        public ActionResult Logout()
        {
            SiteHelper.CurrentAdminUser = null;
            return RedirectToAction("Login");
        }

    }
}