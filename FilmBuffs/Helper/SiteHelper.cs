﻿using FilmBuffs.Areas.Manage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmBuffs.Helper
{
    public class SiteHelper
    {
        public static LoginModel CurrentAdminUser
        {
            get
            {
                return (LoginModel)HttpContext.Current.Session["CurrentAdminUser"];

            }

            set
            {
                HttpContext.Current.Session["CurrentAdminUser"] = value;
            }
        }
    }
}