﻿using FilmBuffs.Models;
using FilmBuffs.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FilmBuffs.Controllers
{
    public class MovieController : BaseController
    {
        // GET: Movie
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Detail(int id)
        {
            if (id > 0)
            {
                var dbContext = new FbDatabase();
                var entity = dbContext.Movies.FirstOrDefault(e => e.Id == id);
                if (entity != null)
                {
                    setPageTitle(entity.Title);

                    Session["MovieId"] = id;

                    return View(new MovieModel(entity));
                }
            }
            return Redirect("/");
        }
    }
}