﻿using FilmBuffs.Database;
using FilmBuffs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FilmBuffs.Controllers
{
    public class ExploreController : Controller
    {
        // GET: Explore
        public ActionResult Index(int? pageIndex, string orderBy)
        {
            if (pageIndex == null || pageIndex < 1)
            {
                pageIndex = 1;
            }

            var model = new PagingModel<MovieModel>
            {
                CurrentPage = pageIndex.Value,
                PageSize = 10,
                OrderBy = orderBy == null ? string.Empty : orderBy.ToLower(),
                Records = new List<MovieModel>()

            };

            var dbContext = new FbDatabase();
            var dbMovies = dbContext.Movies.ToList();
            var moviesFromDatabase = dbContext.Movies.AsEnumerable();

            //ordering
            switch (model.OrderBy)
            {
                case "title":
                    moviesFromDatabase = moviesFromDatabase.OrderByDescending(e => e.Title).ToList();
                    break;
                case "year":
                    moviesFromDatabase = moviesFromDatabase.OrderByDescending(e => e.Year).ToList();
                    break;
                default:
                    moviesFromDatabase = moviesFromDatabase.OrderBy(e => e.Title).ToList();
                    break;
            }


            model.TotalRecord = moviesFromDatabase.Count();
            var dbMovieList = moviesFromDatabase.Skip((model.CurrentPage - 1) * model.PageSize).Take(model.PageSize).ToList();
            model.Records = dbMovieList.Select(e => new MovieModel(e)).ToList();

            return View(model);
        }
    }
}