﻿using FilmBuffs.Database;
using FilmBuffs.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FilmBuffs.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index(int? id)
        {
            var model = new HomeModel
            {
                Heading = "Film Buffs",
                Movies = new List<Models.MovieListModel>()

            };
            var dbContext = new FbDatabase();
            if (id > 0)
            {
                var genre = dbContext.Genres.FirstOrDefault(e => e.Id == id);
                if (genre != null)
                {
                    model.Heading = genre.Name;
                    model.Movies = dbContext.Movies.Where(e => e.GenreID == id).
                                    AsEnumerable().Select(e => new Models.MovieListModel(e)).ToList();
                }
            }
            else
            {
                model.Movies = dbContext.Movies.AsEnumerable().Select(e => new Models.MovieListModel(e)).Take(5).ToList();
                
            }

            return View(model);
        }

        public ActionResult Search (string keyword)
        {

            var model = new SearchModel<MovieModel>
            {
                Keyword = keyword,
                Records = new List<MovieModel>()
            };

            var dbContext = new FbDatabase();
            var dbMovies = dbContext.Movies.ToList();
            var moviesFromDatabase = dbContext.Movies.AsEnumerable();

            if (!string.IsNullOrEmpty(keyword))
            {
                model.Keyword = model.Keyword.Trim().ToLower();
                moviesFromDatabase = moviesFromDatabase.Where(e => e.Title.ToLower().Contains(model.Keyword) ||
                                 e.Id.ToString() == model.Keyword || e.Year.ToString() == model.Keyword).ToList();
            }

            var dbMovieList = moviesFromDatabase.ToList();
            model.Records = dbMovieList.Select(e => new MovieModel(e)).ToList();

            return View(model);
        }

        public ActionResult SidebarPartial()
        {
            var model = new SideBarPartialModel()
            {
                Genres = new List<Models.GenreModel>(),
                RecentlyAdded = new List<Models.MovieListModel>()
            };

            var dbContext = new FbDatabase();
            model.Genres = dbContext.Genres.AsEnumerable().Select(e => new Models.GenreModel(e)).ToList();

            model.RecentlyAdded = dbContext.Movies.
                OrderByDescending(e => e.Id).Take(5).AsEnumerable().Select(e => new Models.MovieListModel(e)).ToList();

            return PartialView(model);
        }
    }
}