﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FilmBuffs.Controllers
{
    public class BaseController : Controller
    {
        // GET: Base
        protected void setPageTitle(string pageTitle)
        {
            ViewBag.Title = pageTitle;
        }
    }
}