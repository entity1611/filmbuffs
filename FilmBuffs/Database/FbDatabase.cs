﻿using FilmBuffs.Database.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace FilmBuffs.Database
{
    public class FbDatabase : DbContext
    {
        public FbDatabase() : base("Default")
        {

        }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<Genre> Genres { get; set; }

    }
}