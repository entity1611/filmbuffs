﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmBuffs.Models
{
    public class PagingModel<T>
    {
        public int TotalRecord { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public int PageNumber
        {
            get
            {
                //if(TotalRecord % PageSize == 0)
                //{
                //    return TotalRecord / PageSize;
                //}
                //else
                //{
                //    return (TotalRecord / PageSize) + 1;
                //}
                return (int)Math.Ceiling((TotalRecord * 1.0) / PageSize);
            }
        }
        public List<T> Records { get; set; }
    }
}