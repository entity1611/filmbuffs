﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmBuffs.Models
{
    public class SideBarPartialModel
    {
        public List<GenreModel> Genres { get; set; }
        public List<MovieListModel> RecentlyAdded { get; set; }
    }
}