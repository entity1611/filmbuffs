﻿using FilmBuffs.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmBuffs.Models
{
    public class MovieModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Year { get; set; }
        public string Director { get; set; }
        public string Plot { get; set; }
        public Genre Genres { get; set; }

        public string GenreName { get; set; }

        public MovieModel() { }

        public MovieModel(Movie entity)
        {
            Id = entity.Id;
            Title = entity.Title;
            Year = entity.Year;
            Director = entity.Director;
            Plot = entity.Plot;
            Genres = entity.Genres;
            GenreName = entity.Genres.Name;
        }       
    }
}