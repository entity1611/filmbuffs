﻿using FilmBuffs.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FilmBuffs.Models
{
    public class MovieListModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Year { get; set; }
        public string Director { get; set; }

        [AllowHtml]
        public string Plot { get; set; }
        //TODO: tags, createdBy, createdTime...

        public MovieListModel() { }

        public MovieListModel(Movie entity)
        {
            Id = entity.Id;
            Title = entity.Title;
            Year = entity.Year;
            Director = entity.Director;
            Plot = entity.Plot;
        }

        public MovieListModel(int id, string title, int year)
        {
            Id = id;
            Title = title;
            Year = year;
        }

    }
}