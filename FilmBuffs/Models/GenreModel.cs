﻿using FilmBuffs.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmBuffs.Models
{
    public class GenreModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public GenreModel() { }

        public GenreModel(Genre entity)
        {
            Id = entity.Id;
            Name = entity.Name;
        }
    }
}