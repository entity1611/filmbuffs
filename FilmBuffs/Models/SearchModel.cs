﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmBuffs.Models
{
    public class SearchModel<MovieModel>
    {
        public string Keyword { get; set; }
        public List<MovieModel> Records { get; set; }
    }
}