﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmBuffs.Models
{
    public class HomeModel
    {
        public string Heading { get; set; }
        public string Keyword { get; set; }
        public List<MovieListModel> Movies { get; set; }
    }
}